#!/bin/bash

mkdir -p /etc/ceph
rm /tmp/ceph* 2>/dev/null

echo "Bringing Ceph configs to devstack node"
scp ceph-primary:/tmp/ceph.conf /tmp/ceph.conf
scp ceph-primary:/tmp/ceph.client.admin.keyring /tmp/ceph.client.admin.keyring
ssh ceph-primary 'sudo rm /tmp/ceph*'
scp ceph-secondary:/tmp/ceph.conf /tmp/ceph-secondary.conf
scp ceph-secondary:/tmp/ceph.client.admin.keyring /tmp/ceph-secondary.client.admin.keyring
ssh ceph-secondary 'sudo rm /tmp/ceph*'
sudo mv /tmp/ceph* /etc/ceph
sudo chmod 0600 /etc/ceph/*.client.admin.keyring
sudo chown vagrant:vagrant /etc/ceph/*.client.admin.keyring

echo "Copying ceph-primary config to the secondary"
scp /etc/ceph/ceph.conf ceph-secondary:/tmp/ceph-primary.conf
scp /etc/ceph/ceph.client.admin.keyring ceph-secondary:/tmp/ceph-primary.client.admin.keyring
ssh ceph-secondary 'sudo mv /tmp/ceph* /etc/ceph; sudo chmod 0600 /etc/ceph/ceph-primary.client.admin.keyring; sudo chown ceph:ceph /etc/ceph/ceph*'

echo "Copying ceph-secondary config to the primary"
scp /etc/ceph/ceph-secondary.conf ceph-primary:/tmp
scp /etc/ceph/ceph-secondary.client.admin.keyring ceph-primary:/tmp
ssh ceph-primary 'sudo mv /tmp/ceph* /etc/ceph; sudo chmod 0600 /etc/ceph/ceph-secondary.client.admin.keyring; sudo chown ceph:ceph /etc/ceph/ceph*'

