#!/bin/bash

my_ip=$1
hostname=`hostname -s`

cd /root

echo "Installing ceph-deploy from pip to avoid existing package conflict"
apt-get update
apt-get install -y python-pip
pip install ceph-deploy

echo "Creating cluster"
ceph-deploy new $hostname --public-network $my_ip/24 || exit 1
echo -e "\nosd pool default size = 1\nosd crush chooseleaf type = 0\n" >> "ceph.conf"

echo "Installing from upstream repository"
echo -e "\n[myrepo]\nbaseurl = http://gitbuilder.ceph.com/ceph-deb-xenial-x86_64-basic/ref/master\ngpgkey = https://download.ceph.com/keys/autobuild.asc\ndefault = True" >> .cephdeploy.conf
ceph-deploy install $hostname || exit 2

echo "Deploying Ceph monitor"
ceph-deploy mon create-initial || exit 3

echo "Copy keyring to /etc/ceph"
cp ceph.client.admin.keyring /etc/ceph
chmod 0600 /etc/ceph/ceph.client.admin.keyring
chown ceph:ceph /etc/ceph/ceph.client.admin.keyring

echo "Copy keyring and configuration to tmp (will be used for copying to other nodes)"
cp -r /etc/ceph/ceph.conf /tmp
chmod 777 /tmp/ceph.conf
cp -r /etc/ceph/ceph.client.admin.keyring /tmp
chmod 777 /tmp/ceph.client.admin.keyring

echo "Creating OSD on /dev/vdb"
ceph-deploy osd create $hostname:/dev/vdb || exit 3

echo "Creating default volumes pool"
ceph osd pool create volumes 100

echo "Enabling Ceph per-image mirroring on volumes pools"
rbd mirror pool enable volumes image

echo "Put Ceph to autostart"
systemctl enable ceph.target
systemctl enable ceph-mon.target
systemctl enable ceph-osd.target

echo "Installing ceph-mirror package and run it as a service"
apt-get install -y rbd-mirror || exit 5
systemctl enable ceph-rbd-mirror@admin
systemctl start ceph-rbd-mirror@admin
