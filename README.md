Scripts and configuration files for cinders ceph replication
automatic deployment with vagrant. More on:
https://gorka.eguileor.com/ceph-replication/ 

STEPS:
1. Create Virtual Machine with Ubuntu 16.04 Linux
8GB RAM , 50GB disk
2. On VM install kvm and libvirt
sudo apt update
sudo apt install qemu-kvm libvirt-bin libvirt-dev
3. On VM create Vagrant and VagrantLibvirt
wget https://releases.hashicorp.com/vagrant/1.9.1/vagrant_1.9.1_x86_64.deb
sudo dpkg -i vagrant_1.9.1_x86_64.deb
vagrant plugin install vagrant-libvirt
4. On VM check if plugin is installed
vagrant plugin list
5. On VM clone this git repository
git clone https://gitlab.com/kemopq/CindersCephReplicationAutDeployment.git
6. On VM run vagrant
cd CindersCephReplicationAutDeployment
sudo chmod 777 /var/run/libvirt/libvirt-sock
vagrant up
7. On VM copy Ceph credentials
vagrant ssh -c '/home/vagrant/copy_ceph_credentials.sh'
8. On VM run devstack
vagrant ssh -c 'cd devstack && ./stack.sh'

