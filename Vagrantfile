# -*- mode: ruby -*-
# vi: set ft=ruby :

USE_LOCAL_DATA = true
REMOTE_PROVIDER = false

VMs = Hash["ceph-primary" => "10.0.1.11", "ceph-secondary" => "10.0.1.12", "devstack" => "10.0.1.10"]


def basic_configuration(vm)
    # Fix Vagrant issue with stdout
    vm.provision :shell , inline: "(grep -q 'mesg n' /root/.profile && sed -i '/mesg n/d' /root/.profile && echo 'Ignore the previous error, fixing this now...') || exit 0;"

    # Make this node know the others and able to seamlessly connect to them
    vm.provision "file", source: "~/.vagrant.d/insecure_private_key", destination: "~vagrant/.ssh/id_rsa"
    vm.provision "shell", inline: "curl -o ~vagrant/.ssh/id_rsa.pub https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub", privileged: false
    vm.provision "shell", inline: "mkdir -m0700 -p ~root/.ssh; cp ~vagrant/.ssh/* ~root/.ssh"
    vm.provision "shell", inline: "
        echo -e '\nHost *\n\tStrictHostKeyChecking no\n\tUserKnownHostsFile /dev/null\n\tLogLevel ERROR\n' >> /root/.ssh/config;
        chmod 644 /root/.ssh/config"
    other_hosts = VMs.select{|k, v| k != vm.hostname}

    hosts_contents = other_hosts.collect do |key, value|
      "#{value} #{key}"
    end.join "\n"
    vm.provision "shell", inline: "echo -e '\n#{hosts_contents}' >> /etc/hosts"
end


Vagrant.configure(2) do |config|
    config.vm.box = "yk0/ubuntu-xenial"
    config.vm.post_up_message = "We are now ready to deploy devstack, you just need to run: vagrant ssh -c 'sudo ~/copy_ceph_credentials.sh; cd devstack; ./stack.sh'"
    config.vm.synced_folder ".", "/vagrant", disabled: true
    config.ssh.insert_key = false
    config.vm.provider :libvirt do |libvirt|
      libvirt.driver = "kvm"
      if REMOTE_PROVIDER
          libvirt.host = "192.168.1.11"
          libvirt.connect_via_ssh = true
          libvirt.username = "root"
      end
    end

    ['ceph-primary', 'ceph-secondary'].each do |node_name|
        config.vm.define node_name do |node|
            node.vm.hostname = node_name
            node.vm.network :private_network, ip: VMs[node_name]

            node.vm.provider :libvirt do |domain|
                domain.memory = 2048
                domain.cpus = 1
                domain.nested = false
                domain.storage :file, :size=> '40G', :device=> 'vdb', :type=> 'qcow2', :bus=> 'virtio', :cache=> 'none'
            end

            basic_configuration(node.vm)

            # Provision ceph cluster
            # If we want to use local file
            if USE_LOCAL_DATA
                node.vm.provision "file", source: "provision_ceph.sh", destination: "~/provision_ceph.sh"
                node.vm.provision "shell", inline: "mv /home/vagrant/provision_ceph.sh /root"
            # To use online version
            else
                node.vm.provision "shell", inline: "curl -o /root/provision_ceph.sh https://gorka.eguileor.com/files/cinder/rbd_replication/provision_ceph.sh"
            end
            # Run the provisioning script
            my_ip = VMs[node_name]
            node.vm.provision "shell", inline: "/bin/bash /root/provision_ceph.sh #{my_ip}"
        end
      end

    config.vm.define "devstack", primary: true do |devstack|
        devstack.vm.hostname = "devstack"
        devstack.vm.network :private_network, ip: VMs['devstack']

        devstack.vm.provider :libvirt do |domain|
            domain.memory = 4096
            domain.cpus = 2
            domain.nested = true
            domain.volume_cache = 'none'
        end

        basic_configuration(devstack.vm)

        # Install ceph common
        devstack.vm.provision "shell", inline: "
            echo -e '\ndeb http://gitbuilder.ceph.com/ceph-deb-xenial-x86_64-basic/ref/master xenial main' >> /etc/apt/sources.list;
            apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 6EAEAE2203C3951A;
            apt-get update;
            apt-get install -y ceph-common python-dev rbd-mirror"


        # # Clone devstack
        devstack.vm.provision "shell", inline: "git clone https://git.openstack.org/openstack-dev/devstack", privileged: false

        # Copy Ceph credential copying script and devstack base configuration and post stacking script
        if USE_LOCAL_DATA
            devstack.vm.provision "file", source: "copy_ceph_credentials.sh", destination: "~/copy_ceph_credentials.sh"
            devstack.vm.provision "file", source: "local.conf", destination: "~/devstack/local.conf"
            devstack.vm.provision "file", source: "local.sh", destination: "~/devstack/local.sh"
        # To use online version
        else
            devstack.vm.provision "shell", inline: "
            curl -O https://gorka.eguileor.com/files/cinder/rbd_replication/copy_ceph_credentials.sh;
            cd devstack;
            curl -O https://gorka.eguileor.com/files/cinder/rbd_replication/local.conf;
            curl -O https://gorka.eguileor.com/files/cinder/rbd_replication/local.sh", privileged: false
        end
        devstack.vm.provision "shell", inline: "chmod +x ~/devstack/local.sh ~/copy_ceph_credentials.sh", privileged: false
    end
end
